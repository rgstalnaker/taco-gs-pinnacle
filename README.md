# GoonSwarm - Ascendance - Pinnacle TACO #
**An eye candy filled, multi-threaded, max architecture, optimized, deep learning, swarm enabled, self-healing intel channel monitor with an OpenGL based map display.**

Pinnacle TACO is a next-generation smart application system based on T.A.C.O. Reloaded.  Users will experience the combination of a mouth watering UI, blisteringly fast and accurate data processing, and an intelligent suggestive UX backed by deep learning powered informatics.

## To Get Started ##

*Programmers*

[Check the JIRA site](https://taco-gs-pinnacle.atlassian.net/) to find **how you can help** and what the prioritization for tasks/bugs are.
  **Clone** the repository.  **Create** bug and/or feature patches.  **Commit** your well commented changes as requested on the JIRA site.  Submit **Pull** requests to have your code reviewed.  Acceptable changes will be merged into the applicable branch.

*Testers*

Pinnacle Server - System requirements and instructions will be posted during the Version 0.1 Alpha test preparation and prior to the test cycle go-live date.

TACO Client - System requirements and instructions will be posted during the Version 0.1 Alpha test preparation and prior to the test cycle go-live date.